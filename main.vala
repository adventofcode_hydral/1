public class Elf{
	public Elf(int id, string inventory){
		m_id = id;
		m_callories = 0;
		var split = inventory.split("\n");
		foreach (var calorie in split)
			m_callories += int.parse(calorie);
	}
	public static Elf []create_all_elf(string []split)
	{
		Elf []elfs = {};
		var id = 1;
		
		foreach (var inventory in split)
			elfs += new Elf(id++, inventory);
		return elfs;
	}
	public static int get_elfs_n_most_callories(Elf []elfs, int nb)
	{
		int []tab_copy = null;
		int []sum = null;
		
		foreach (var elf in elfs)	
			tab_copy += elf.m_callories;
		for (var n = 0 ; n != nb; n++)
		{
			sum += get_max_from_tab(tab_copy);
			for (var i = 0; i != tab_copy.length; i++)
				if (tab_copy[i] == sum[sum.length - 1])
					tab_copy[i] = 0;

		}
		return sum[0] + sum[1] + sum[2];
	}
	public static int get_elf_with_most_callories(Elf []elfs)
	{
		var max = 0;
		foreach (var item in elfs)
			if (item.m_callories > max)
				max = item.m_callories;
		return max;
	}
	public int m_callories { private set; get;}
	public int m_id { private set; get;}
}

public static int get_max_from_tab(int []tab)
{
	int max = 0;
	foreach (var i in tab)
		if (max < i)
			max = i; 
	return max;
}

int	main(string []args)
{
	string txt;
	FileUtils.get_contents("input.txt", out txt);
	var split =  txt.split("\n\n");
	var elfs = Elf.create_all_elf(split);

	//part 1
	var max_calories = Elf.get_elf_with_most_callories(elfs);
	//part 2
	var max_calories_top3 = Elf.get_elfs_n_most_callories(elfs, 3);
	
	print(@"part1: $max_calories\npart2: $max_calories_top3");
	return (0);
}
